import discord
import asyncio
import requests

class MyClient(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # create the background task and run it in the background
        self.bg_task = self.loop.create_task(self.my_background_task())

    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')

    async def my_background_task(self):
        await self.wait_until_ready()
        
        rush = self.get_channel(460520708414504961)
        incentives = [[461206320092282911, '3015', 147]]
        
        #template = "Upcoming: `{}` by {} (Est. {})\n".format('Dragon Warrior III (Any%)', 'vaxherd', '1:20:00') # ✨
        #template += "⚠ Donation Incentive: {} (${} out of ${})".format('Slime Ending Glitch', '7,240', '7,500')
        #await rush.send(template)
        
        #message = await rush.get_message(460912625690214402)
        #await message.edit(content=message.content+"\n<http://twitch.tv/videos/277317243?t=20h27m56s>")
        
        while not self.is_closed():
            for incentive in incentives:
                message = await rush.get_message(incentive[0])
                run_number = incentive[1]
                content = message.content
                money_list = []
                floatmoney = []
                
                r = requests.get('https://gamesdonequick.com/tracker/run/' + run_number)
                lines = r.text.split('\n')
                
                try:
                    line_number = incentive[2]
                    money_list.append(lines[line_number])
                    money_list.append(lines[line_number+3])
                except IndexError:
                    for line in lines:
                        if line.startswith('$') and len(money_list) < 2:
                            money_list.append(line)
                
                for stringmoney in money_list:
                    floatmoney.append(float(stringmoney.replace('$', '').replace(',','')))
                    
                money = money_list[0]
                donation_met = floatmoney[0] >= floatmoney[1]
                    
                if donation_met and '⚠' in content:
                    content = content.replace('⚠', '✅').replace('Upcoming', '~~Upcoming').replace('\n','~~\n')
                    
                edit = content.replace('\n', ' ').split(' ')[::-1]
                edit[3] = '(' + money
                edit = ' '.join(edit[::-1]).replace(' ⚠', '\n⚠').replace(' ✅', '\n✅')
            
                await message.edit(content=edit)
                
                if donation_met:
                    print(f"donation met, please remove {incentive[0]} from list")
                    incentives.remove(incentive) # probably won't raise a ValueError because list wasn't edited
            
            await asyncio.sleep(60) # task runs every 60 seconds


client = MyClient()
client.run(open("rush-token.txt").read().split("\n")[0])
